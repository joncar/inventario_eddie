<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function inventario(){
            $inventario = $this->db->get_where('notif');
            $inv = array();
            foreach($inventario->result() as $i){
                $inv[] = $i;
            }
            echo json_encode($inv);
        }    
    }
?>
